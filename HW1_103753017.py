# -*- coding: utf-8 -*-
# Q1
from PIL import Image
src=r'/Users/wang/python/nhw1/Penguins.jpg'
im = Image.open(src)
pixel = im.load() # 回傳像素
width, height = im.size # 得到影像的寬度長度

for x in range(0,width*1/3):
 for y in range(height):
  cpixel = pixel[x,y] # 取得該pixel的值RGB
  luma = (3 * cpixel[0],1*cpixel[1] ,1*cpixel[2] )
  pixel[x,y]=luma

for x in range(width/3,width*2/3):
 for y in range(height):
  cpixel = pixel[x,y] # 取得該pixel的值RGB
  luma = (1 * cpixel[0],3*cpixel[1] ,1*cpixel[2] )
  pixel[x,y]=luma

for x in range(width*2/3,width):
 for y in range(height):
  cpixel = pixel[x,y] # 取得該pixel的值RGB
  luma = (1 * cpixel[0],1*cpixel[1] ,3*cpixel[2] )
  pixel[x,y]=luma
im.show()
im.save("Q1.jpg")

###########

#Q2
from PIL import Image
src=r'/Users/wang/python/nhw1/Penguins.jpg'
im = Image.open(src)
im=im.convert('CMYK')
#im.show()
pixel = im.load() # 回傳像素
#print pixel[0,0]
width, height = im.size # 得到影像的寬度長度
step=width/4
for x in range(0,step):
 for y in range(height):
  cpixel = pixel[x,y] # 取得該pixel的值RGB
  luma = (3 * cpixel[0],1*cpixel[1] ,1*cpixel[2] ,1*cpixel[3])
  pixel[x,y]=luma

####
for x in range(step,step*2):
 for y in range(height):
  cpixel = pixel[x,y] # 取得該pixel的值RGB
  luma = (1 * cpixel[0],3*cpixel[1] ,1*cpixel[2],1*cpixel[3] )
  pixel[x,y]=luma
###
for x in range(step*2,step*3):
 for y in range(height):
  cpixel = pixel[x,y] # 取得該pixel的值RGB
  luma = ( cpixel[0],cpixel[1] ,3*cpixel[2],cpixel[3] )
  pixel[x,y]=luma
###
for x in range(step*3,width):
 for y in range(height):
  cpixel = pixel[x,y] # 取得該pixel的值RGB
  luma = ( cpixel[0],cpixel[1] ,cpixel[2],3*cpixel[3] )
  pixel[x,y]=luma
im.show()
im.save('Q2.jpg')

##########

#Q3
from PIL import Image
src=r'/Users/wang/python/nhw1/Penguins.jpg'
im = Image.open(src)

#im.show()
pixel = im.load() # 回傳像素
#print pixel[0,0]
width, height = im.size # 得到影像的寬度長度

for x in range(width):
 for y in range(height):
  r,g,b = pixel[x,y] # 取得該pixel的值RGB
  temp = (255-r,255-g,255-b)
     
        
  pixel[x,y]=temp   
im.show()
im.save('Q3.jpg')

#####################

#Q4
from PIL import Image
src=r'/Users/wang/python/nhw1/Penguins.jpg'
im = Image.open(src)
#im.show()
pixel = im.load() # 回傳像素
width, height = im.size # 得到影像的寬度長度
for x in xrange(0,width,8):
 for y in xrange(0,height,8):
  cpixel = pixel[x,y] # 取得該pixel的值RGB
  for x1 in xrange(0,8,1): 
     for y1 in xrange(0,8,1):
        pixel[x+x1,y+y1]=pixel[x,y]
im.show()
im.save('Q4.jpg')

####################

#Q5
from PIL import Image
src=r'/Users/wang/python/nhw1/Penguins.jpg'
im = Image.open(src)
#im.show()
pixel = im.load() # 回傳像素
width, height = im.size # 得到影像的寬度長度
#print im.size
for x in xrange(0,width):
 for y in xrange(0,height):
  cpixel = pixel[x,y] # 取得該pixel的值RGB
  gray1=(cpixel[0]+cpixel[1]+cpixel[2])/3
  luma = (gray1,gray1,gray1)
  pixel[x,y]=luma

im.show()
im.save('Q5-1.jpg')


im = Image.open(src)
#im.show()
pixel = im.load() # 回傳像素
width, height = im.size # 得到影像的寬度長度
for x in xrange(0,width):
 for y in xrange(0,height):
  r,g,b = pixel[x,y] # 取得該pixel的值RGB
  gray1=int (0.299*r+0.587*g+0.114*b)
  luma = (gray1,gray1,gray1)
  pixel[x,y]=luma
im.show()
im.save('Q5-2.jpg')

#######################

#Q6

# -*- coding: utf-8 -*-
from PIL import Image
src=r'/Users/wang/python/nhw1/Penguins.jpg'
im = Image.open(src)
#im.show()
pixel = im.load() # 回傳像素
width, height = im.size # 得到影像的寬度長度
#print im.size
for x in xrange(0,width):
 for y in xrange(0,height):
  r,g,b = pixel[x,y] # 取得該pixel的值RGB
  if (r+g+b)/3 < 20:
      pixel[x,y]=(0,0,0)
  else:
      pixel[x,y]=(255,255,255)

im.show()
im.save('Q6-1.jpg')

##
im2 = Image.open(src)
pixel = im2.load() # 回傳像素
width, height = im2.size # 得到影像的寬度長度
#print im.size
for x in xrange(0,width):
 for y in xrange(0,height):
  r,g,b = pixel[x,y] # 取得該pixel的值RGB
  if (r+g+b)/3 < 64:
      pixel[x,y]=(0,0,0)
  else:
      pixel[x,y]=(255,255,255)
im2.show()
im.save('Q6-2.jpg')

im3 = Image.open(src)
pixel = im3.load() # 回傳像素
width, height = im3.size # 得到影像的寬度長度
#print im.size
for x in xrange(0,width):
 for y in xrange(0,height):
  r,g,b = pixel[x,y] # 取得該pixel的值RGB
  if (r+g+b)/3 < 180:
      pixel[x,y]=(0,0,0)
  else:
      pixel[x,y]=(255,255,255)
im3.show()
im.save('Q6-3.jpg')

##############################

#Q7
from PIL import Image
src=r'/Users/wang/python/nhw1/Penguins.jpg'
im = Image.open(src)
#im.show()
pixel = im.load() # 回傳像素
width, height = im.size # 得到影像的寬度長度
#print im.size
for x in xrange(0,width):
 for y in xrange(0,height):
  R,G,B = pixel[x,y] # 取得該pixel的值RGB
  pixel[x,y]=(R,B,G)
im.show()
im.save('Q7-1.jpg')

im = Image.open(src)
#im.show()
pixel = im.load() # 回傳像素
width, height = im.size # 得到影像的寬度長度
#print im.size
for x in xrange(0,width):
 for y in xrange(0,height):
  R,G,B = pixel[x,y] # 取得該pixel的值RGB
  pixel[x,y]=(G,R,B)
im.show()
im.save('Q7-2.jpg')
##

im = Image.open(src)
#im.show()
pixel = im.load() # 回傳像素
width, height = im.size # 得到影像的寬度長度
#print im.size
for x in xrange(0,width):
 for y in xrange(0,height):
  R,G,B = pixel[x,y] # 取得該pixel的值RGB
  pixel[x,y]=(G,B,R)
im.show()
im.save('Q7-3.jpg')

im = Image.open(src)
#im.show()
pixel = im.load() # 回傳像素
width, height = im.size # 得到影像的寬度長度
#print im.size
for x in xrange(0,width):
 for y in xrange(0,height):
  R,G,B = pixel[x,y] # 取得該pixel的值RGB
  pixel[x,y]=(B,R,G)
im.show()
im.save('Q7-4.jpg')
##

im = Image.open(src)
#im.show()
pixel = im.load() # 回傳像素
width, height = im.size # 得到影像的寬度長度
#print im.size
for x in xrange(0,width):
 for y in xrange(0,height):
  R,G,B = pixel[x,y] # 取得該pixel的值RGB
  pixel[x,y]=(B,G,R)
im.show()
im.save('Q7-5.jpg')

############################

#Q8
from PIL import Image
import math
src=r'/Users/wang/python/nhw1/Penguins.jpg'
im= Image.open(src)
#im.show()
pixel=im.load() # 回傳像素
width, height = im.size # 得到影像的寬度長度
def distance(a, b):
    return math.sqrt((a[0] - b[0]) * (a[0] - b[0]) + (a[1] - b[1]) * (a[1] - b[1]) + (a[2] - b[2]) * (a[2] - b[2]) )

for x in range(width):
    for y in range(height):
       px = pixel[x,y]
       luma = ( px[0], int(0.6* px[1]), px[2] )
       #
       if distance(px,(255,190,25)) < 100 :
             #print distance(px,(255,190,25))
             #print pixel[x,y]
             pixel[x,y] =luma
im.show()  
im.save('Q8.jpg')

###############################
#Q9

from PIL import Image

src=r'/Users/wang/python/nhw1/Penguins.jpg'
im = Image.open(src)
pixel = im.load() # 回傳像素
width, height = im.size # 得到影像的寬度長度
#print im.size
for x in range(0,width-1,):
 for y in range(0,height-1,):
  px = pixel[x,y] # 取得該pixel的值RGB
  px1= pixel[x+1,y]
  py1= pixel[x,y+1]
  rgb_x=(px[0]+px[1]+px[2])/3
  rgb_x1=(px1[0]+px1[1]+px1[2])/3
  rgb_y1=(py1[0]+py1[1]+py1[2])/3  
  if abs(rgb_x-rgb_x1) >10 and abs(rgb_x-rgb_y1)>10:
     #pixel[x,y] = (255,255,255)         
     pixel[x,y] = (0,0,0) 
  else: 
     pixel[x,y] = (255,255,255)             
     #pixel[x,y] = (0,0,0) 

im.show()
im.save('Q9.jpg')

###################################

#Q10

from PIL import Image

src=r'/Users/wang/python/nhw1/Penguins_noise.jpg'
im = Image.open(src)
im.show()
#new=Image.new('RGB', im.size, (0, 0, 0)) 
px = im.load() # 回傳像素
width, height = im.size # 得到影像的寬度長度
#print im.size
a=[0 for i in range(0,9)]
rgb=[0,0,0]
#
for x in range(1,width-1,1):
 for y in range(1,height-1,1):
    #print px[x,y]
   for i in range(0,3): 
    a[0]=  px[x-1,y-1][i]
    a[1]=  px[x  ,y-1][i]
    a[2]=  px[x+1,y-1][i]
    a[3]=  px[x-1,y][i]
    a[4]=  px[x  ,y][i]
    a[5]=  px[x+1,y][i]
    a[6]=  px[x-1,y+1][i]
    a[7]=  px[x  ,y+1][i]
    a[8]=  px[x+1,y+1][i]
    a.sort()
    rgb[i]=a[4]
    #print rgb
   px[x,y]=(rgb[0],rgb[1],rgb[2])    
 

im.show()
im.save('Q10.jpg')

##############################
#Q11

#右到左
from PIL import Image
src=r'/Users/wang/python/nhw1/Penguins.jpg'
im = Image.open(src)
pixel = im.load() # 回傳像素
width, height = im.size # 得到影像的寬度長度
#im.show()
#print im.size
for x in xrange(0,width/2):
 for y in xrange(0,height):
  pixel[width-x-1,y]=pixel[x,y] 
im.show()
im.save('Q11-1.jpg')

##
#左到右
im = Image.open(src)
pixel = im.load() # 回傳像素
width, height = im.size # 得到影像的寬度長度
#im.show()
#print im.size
for x in xrange(0,width/2):
 for y in xrange(0,height):
  pixel[x,y] =pixel[width-x-1,y]
im.show()
im.save('Q11-2.jpg')

##
#上到下
im = Image.open(src)
pixel = im.load() # 回傳像素
width, height = im.size # 得到影像的寬度長度
#im.show()
#print im.size
for x in xrange(0,width):
 for y in xrange(0,height/2):
  pixel[x,height-y-1] =pixel[x,y]
im.show()
im.save('Q11-3.jpg')

#
#下到上
im = Image.open(src)
pixel = im.load() # 回傳像素
width, height = im.size # 得到影像的寬度長度
#im.show()
#print im.size
for x in xrange(0,width):
 for y in xrange(0,height/2):
  pixel[x,y] =pixel[x,height-y-1]
im.show()
im.save('Q11-4.jpg')

#################################

#Q12
from PIL import Image
import math
src=r'/Users/wang/python/nhw1/Elsa.jpg'
dst=r'/Users/wang/python/nhw1/Chrysanthemum.jpg'
im = Image.open(src)
flower=Image.open(dst)
pixel = im.load() # 回傳像素
px_flower = flower.load() # 回傳像素
width, height = im.size # 得到影像的寬度長度
def distance(a, b):
    return   math.sqrt((a[0] - b[0]) * (a[0] - b[0]) + (a[1] - b[1]) * (a[1] - b[1]) + (a[2] - b[2]) * (a[2] - b[2]) )
for x in range(0,width):
 for y in range(0,height):
  #
   if distance(pixel[x,y],(29,18,87)) < 20 :
    pixel[x,y]=px_flower[x,y]

im.show()
im.save('Q12.jpg')




